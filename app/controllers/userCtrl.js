angular.module('ccadmin')
.controller('userCtrl', ['$rootScope', '$scope', '$http', '$state', 'usersService', function ($rootScope, $scope, $http, $state, usersService) {

	$scope.formState = "Add";
	
	$scope.user = {
			id: "",
			first_name:"",
			last_name:"",
			phone:"",
			email:"",
			password:"",
			login_type:"",
			type:"",
			address_id:"",
			location_id:""
	};

	$scope.addUser = function(){
		if($scope.user.id){
			$scope.updateUser();
		} else {
			console.log("this is the user object we are adding which we got from the form");
			console.log($scope.user);
			usersService.addUser($scope.user).then(function(res){
				$scope.allUserData.push($scope.user);
				$scope.user = {
					id: "",
					first_name:"",
					last_name:"",
					phone:"",
					email:"",
					password:"",
					login_type:"",
					type:"",
					address_id:"",
					location_id:""
				};
				$("#collapseForm").collapse("hide");
			}, function(err){
				console.log("error in adding user");
			});
		}
	}


	$scope.allUserData = [];

	$scope.editUser = function(user){
		$scope.user = {
			id:user.id,
			first_name:user.first_name,
			last_name:user.last_name,
			phone:user.phone,
			email:user.email,
			password:user.password,
			login_type:user.login_type,
			type:user.type,
			address_id:user.address_id,
			location_id:user.location_id
		};
		$("#collapseForm").collapse("show");
		$scope.formState = "Edit";
	}
	
	$scope.updateUser = function(){
		usersService.updateUser($scope.user, $scope.user.id).then(function(res){
			$scope.user = {};
		}, function(err){
			console.log("error in updating user");
		});
	}
	
	$scope.getAllUsers = function(){
		usersService.getAllUsers().then(function(res){
			console.log(res);
			$scope.allUserData = res.data.data;
		}, function(err){
			console.log("error in getting user");
		});
	}

	$scope.getAllUsers();

	$scope.getUserDetails = function(userId){
		angular.forEach($scope.allUserData, function(user) {
		  if(user.id == userId){
		  	$scope.user = user;
		  }
		});
	}

	$scope.openDeleteConfirmModal = function(user){
		$scope.tempId = user.id;
		$("#confirmDelete").modal("show");
	}

	$scope.deleteUser = function(){
		console.log($scope.tempId);
		usersService.deleteUser($scope.tempId).then(function(res){
			$scope.getAllUsers();
			$("#confirmDelete").modal("hide");
		}, function(err){
			console.log("error in deleting user");
		});
	}
}])