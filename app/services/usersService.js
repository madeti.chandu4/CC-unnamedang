angular.module('ccadmin')
.service('usersService',['$q', '$http', '$rootScope', function($q, $http, $rootScope) {
  return {
    addUser: function(data){
      var defer = $q.defer();
      $http.post($rootScope.host+"/api/v1/user",data).then(function(res){
        defer.resolve(res);
      }, function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    updateUser: function(data,id){
      var defer = $q.defer();
      $http.post($rootScope.host+"/api/v1/user/"+id,data).then(function(res){
        defer.resolve(res);
      }, function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    deleteUser: function(id){
      var defer = $q.defer();
      $http.delete($rootScope.host+"/api/v1/user/"+id).then(function(res){
        defer.resolve(res);
      }, function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    getAllUsers: function(){
      var defer = $q.defer();
      $http.get($rootScope.host+"/api/v1/users").then(function(res){
        defer.resolve(res);
      }, function(err){
        defer.reject(err);
      });
      return defer.promise;
    },
    getUserById: function(id){
      var defer = $q.defer();
      $http.get($rootScope.host+"/api/v1/user/"+animalId).then(function(res){
        defer.resolve(res);
      }, function(err){
        defer.reject(err);
      });
      return defer.promise;
    }
  };
}]);