angular.module('ccadmin',
    [
        'ui.router',
        'ui.bootstrap',
        'ngSanitize',
    ])

    .config(['$locationProvider', '$stateProvider', '$urlRouterProvider',
        function ($locationProvider, $stateProvider, $urlRouterProvider) {
            $urlRouterProvider.when("", "/");
            $urlRouterProvider.otherwise("/");
            
            $stateProvider
            .state('user',{
                url: "/user",
                views: {
                    '': {
                        templateUrl: "templates/user.html",
                         controller: "userCtrl"
                    }
                }
            })
        }])
    .run(['$rootScope', '$timeout', '$state', '$window', 
        function($rootScope, $timeout, $state, $window){

          $rootScope.host = "http://localhost:8000";
     //   $rootScope.host = "https://hungreeapi.sukor.in";
     //   $rootScope.imagePath = "https://s3.ap-south-1.amazonaws.com/hungreehyderabad/";
    
        }
    ]);
