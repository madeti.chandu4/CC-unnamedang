/*
 * Express router for serving templates
 * Team Sukor
 */

 const h  = require('http'),
 	   e  = require('express'),
 	   f  = require('fs'),
 	   bp = require('body-parser'),
 	   cp = require('cookie-parser');

module.exports.unleash = function () {
	var port = process.env.PORT | 6001;
	var app = e();
	var api = e();
	var r = e.Router();
	var apir = e.Router();

	app.use(e.static(__dirname + '/app'));
	app.use(function(req, res, next) {
		res.header("Access-Control-Allow-Origin", "*");
		res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		next();
	});
	app.set('title', 'CC Admin | The UI');
	api.set('title', 'CC Admin | The API');
	app.use(e.static('assets'));
	app.use(cp());
	app.use(bp.json());
	app.use(bp.urlencoded({extended: true}));


	r.get('/', function (req, res) {
		res.sendFile('index.html');
		// stuff comes here
	});


	

	h.createServer(app).listen(port, function () {
		console.log('Front end server up at http://localhost:/' + port);
	});
}
